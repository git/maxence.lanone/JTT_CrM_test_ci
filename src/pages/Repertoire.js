import NavigationDashboard from '../components/NavigationDashboard';
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import user from '../images/user.jpg';
import { TableContainer, Table, TableHead, TableBody, TableRow, TableCell } from '@mui/material';
import { Paper } from '@mui/material';
import { NavLink } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import Session from 'react-session-api';

const api = axios.create({
    baseURL: 'http://localhost:8080'
})

function Repertoire() {

    const [theme, setTheme] = useState("light");
    if (localStorage.getItem('theme') && localStorage.getItem("theme") !== '' && localStorage.getItem("theme") !== theme) {
        setTheme(localStorage.getItem("theme"))
    }
    const [contactSelectionne, setContactSelectionne] = useState("");
    const [modifContact, setModifContact] = useState(false);
    const [showConfirmation, setShowConfirmation] = useState(false);
    const [contacts, setContacts] = useState([]);
    const [SearchTerm, setSearchTerm] = useState("");
    const [SearchResults, setSearchResults] = useState([]);
    const [customers, setCustomers] = useState([]);

    
    useEffect(() => {
        console.log("useEffect appel api contact");
        const apiString = '/Contact/' + Session.get("idUser");
        api.get(apiString).then((response) => {

            setContacts(response.data);
            setSearchResults(response.data);
            console.log("response.data", response.data);
            
        });
    }, [null,contactSelectionne]);

    useEffect(() => {
        api.get('/Customer/All').then((response) => {
            setCustomers(response.data);
        });
    }, []);

    useEffect(() => {

        const results = contacts.filter(contact =>
            contact.lastname.toLowerCase().includes(SearchTerm.toLowerCase())
            || contact.firstname.toLowerCase().includes(SearchTerm.toLowerCase())
            || contact.mail.toLowerCase().includes(SearchTerm.toLowerCase())
            || contact.phone.toLowerCase().startsWith(SearchTerm.toLowerCase())
            || contact.name.toLowerCase().includes(SearchTerm.toLowerCase())
            || SearchTerm.trim().length === 0
        );
        setSearchResults(results);
    }, [null, SearchTerm, modifContact]);

    function handleDeleteClick(contact) {
        api.delete('/Contact/Delete/' + contact.idcontact).then((response) => {
            const newContacts = contacts.filter((c) => c.idcontact !== contact.idcontact);
            setContacts(newContacts);
        });
        setContactSelectionne("");   
        setShowConfirmation(false);
    }

    

    function ContactForm(props) {
        const [lastname, setLastname] = useState(props.contact.lastname);
        const [firstname, setFirstname] = useState(props.contact.firstname);
        const [mail, setMail] = useState(props.contact.mail);
        const [phone, setPhone] = useState(props.contact.phone);
      
        const handleSubmitModif = (event) => {
            event.preventDefault()
            const contact = {
                lastname: lastname,
                firstname: firstname,
                mail: mail,
                phone: phone,
                idcontact: props.contact.idcontact,
            };
            api.put('/Contact/Update/' + props.contact.idcontact, contact).then((response) => {});
            setModifContact(false);
            setContactSelectionne(contact);
        };
      
        return (
          <form onSubmit={handleSubmitModif}>
            <label>
              Nom :
              <input
                type="text"
                value={lastname}
                onChange={(event) => setLastname(event.target.value)}
                required
              />
            </label>
            <br />
            <label>
              Prénom :
              <input
                type="text"
                value={firstname}
                onChange={(event) => setFirstname(event.target.value)}
                required
              />
            </label>
            <br />
            <label>
              Mail :
              <input
                type="email"
                value={mail}
                onChange={(event) => setMail(event.target.value)}
                required
              />
            </label>
            <br />
            <label>
              Numéro de téléphone :
              <input
                type="tel"
                value={phone}
                onChange={(event) => setPhone(event.target.value)}
                pattern="[0-9]{10}" required 
              />
            </label>
            <button type="submit">Envoyer</button>
            <button type="button" onClick={() => setModifContact(false)}>
              Annuler
            </button>
          </form>
        );
    }
    
    return (
        <body className={theme}>
            <div className="page_repertoire">
                {/* Create an account page */}
                <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.0/css/line.css"></link>
                <div className="haut_de_page">
                    <h2 className="titre">Repertoire</h2>
                    <div className="rechLogo">
                        <img className="logo" srcSet={theme === "light" ? './LogoApp.svg' : './LogoApp_light.svg'}/>
                    </div>
                </div>
                <div className="bas_de_page">
                    <NavigationDashboard />
                    <div className="contenu">
                        <span className="searchAndAddButton">
                            { contactSelectionne === "" ?
                                <div className="input_box">
                                    <input type="text" placeholder="Rechercher" value={SearchTerm} onChange={(event) => setSearchTerm(event.target.value)} />
                                    <span className="search">
                                        <i class="uil uil-search search-icon"></i>
                                    </span>
                                </div>
                            :
                                <button className="boutonRetour" onClick={() => {setModifContact(false);setContactSelectionne("");setShowConfirmation(false)}}>Retour</button>
                            }
                            { contactSelectionne === "" ?
                            
                                <NavLink to="/Repertoire/add">
                                    <button className="boutonAddContact">Ajouter</button>
                                </NavLink>
                            :<div hidden></div>}
                        </span>
                        { contactSelectionne === "" ? 
                            <TableContainer component={Paper} className="tabListContact" style={{color: '#fff'}}>
                                <Table>
                                    <TableHead>
                                        <TableRow className="headerListe">
                                            <TableCell>Photo</TableCell>
                                            <TableCell>Nom</TableCell>
                                            <TableCell>Prénom</TableCell>
                                            <TableCell>Téléphone</TableCell>
                                            <TableCell>Entreprise</TableCell>   
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {SearchResults.map((contact) => (
                                            <TableRow key={contact.idcontact} onClick={() => setContactSelectionne(contact)}>
                                                <TableCell><img className="photoContact" src={user} /></TableCell>
                                                <TableCell>{contact.lastname}</TableCell>
                                                <TableCell>{contact.firstname}</TableCell>
                                                <TableCell>{contact.phone}</TableCell>
                                                <TableCell>{contact.name}</TableCell>
                                            </TableRow>
                                        ))}
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        : !modifContact ?
                            
                            <div>
                                <h2>{contactSelectionne.lastname} {contactSelectionne.firstname}</h2>
                                <p>Mail: {contactSelectionne.mail}</p>
                                <p>Numéro de téléphone: {contactSelectionne.phone}</p>
                                <p>Entreprise: {contactSelectionne.name}</p>
                                {!showConfirmation ?
                                <div>
                                    <button className="boutonSupprimer" onClick={() => setShowConfirmation(true)}>Supprimer</button>
                                    <button className="boutonModifier" onClick={() => setModifContact(true)}>Modifier</button>
                                </div>
                                :
                                <div>
                                    <p>Êtes-vous sûr de vouloir supprimer ce contact ?</p>
                                    <button className="Annuler" onClick={() => setShowConfirmation(false)}>Annuler</button>
                                    <button className="boutonSupprimer" onClick={() => handleDeleteClick(contactSelectionne)}>Supprimer</button>
                                </div>
                                }
                            </div>
                        :
                            <ContactForm contact={contactSelectionne} setModifContact={setModifContact} />
                        }
                    </div>
                </div>
            </div>
        </body>
    );
};

function SupprimerContact(props) {

    const navigate = useNavigate();
    const deleteContact = (id) => {
        api.delete('/Contact/' + id).then(() => {
            props.getContactList();
        });
    };

    return (
        <div className="ui main">
            <h2>Supprimer Contact</h2>
            <button className="ui button blue" onClick={() => deleteContact(props.id)}>Supprimer</button>
        </div>
    );
}



export default Repertoire;