import React, { useState, useEffect } from 'react';
import NavigationDashboard from '../components/NavigationDashboard';
import Session from 'react-session-api';
import axios from 'axios';

const api = axios.create({
    baseURL: 'http://localhost:8080'
})

function Dashboard() {

    const [infoContactRecent, setInfoContactRecent] = useState([]);
    const [dataKey, setDataKey] = useState([]);
    const [infoBestCustomer, setinfoBestCustomer] = useState([]);
    const [data12, setData12] = useState([]);
    const [contact, setContact] = useState([]);

    const [theme, setTheme] = useState("light");
    if (localStorage.getItem('theme') && localStorage.getItem("theme") !== '' && localStorage.getItem("theme") !== theme) {
        setTheme(localStorage.getItem("theme"))
    }

    useEffect(() => {

        const date = new Date();
        date.setMonth(date.getMonth() - 1);

        const year = date.getFullYear();
        const month = (date.getMonth() + 1);

        const apiString = '/Contact/LastAdd3/' + Session.get("idUser");
        api.get(apiString).then((response) => {
            const data = response.data;
            if (data.length > 0){
                setInfoContactRecent(response.data[0]);
            }
            else{
                setInfoContactRecent("");
            }
        });

        const apiStringKey = '/Sale/KeyNumber/' + Session.get("idUser") + "/" + month + "/" + year;
        api.get(apiStringKey).then((response) => {
            const data = response.data;
            if (data.length > 0){
                setDataKey(response.data[0]);
            }
            else{
                setDataKey("");
            }
        });

        const apiStringBestCustomer = '/Sale/BestCustomer/' + Session.get("idUser");
        api.get(apiStringBestCustomer).then((response) => {
            if (response.data.length > 0)
                setinfoBestCustomer(response.data[0]);
            else
                setinfoBestCustomer({name :"Aucun client",total: "0"});
        });
        
        const date2 = new Date();
        const nbMonths = 12;
        date2.setMonth(date2.getMonth() - nbMonths);
        const month12 = (date2.getMonth() + 1);
        const apiStringLine = '/Sale/Line/' + Session.get("idUser") + '/' + month12 + '/' + year;
        console.log(apiStringLine);
        api.get(apiStringLine).then((response) => {
            setData12(response.data);
            console.log(response.data);
        });

        const apiStringGetUser = '/User/Id/' + Session.get("idUser");
        api.get(apiStringGetUser).then((response) => {
            setContact(response.data[0]);
            console.log(response.data);
        });
        // setTimeout(() => {}, 500)

    }, []);
    if(data12.length > 0){
    console.log((data12[8].total+data12[9].total+data12[10].total)/3 )
    console.log(data12[11].total)}
    return (
        <body className={theme}>

            <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.0/css/line.css"></link>

            <div className="page_dashboard">
                {/* Create an account page */}
                <div className="haut_de_page">
                    <h2 className="titre">Dashboard</h2>
                    <div className="rechLogo">
                        <img className="logo" srcSet={theme === "light" ? './LogoApp.svg' : './LogoApp_light.svg'} />
                    </div>
                </div>
                <div className="bas_de_page">
                    <NavigationDashboard />
                    <div className="Dashboard">
                        <div className="_gauche">
                            <div className="_haut">
                                <div className="Mes_infos">
                                    <p className='titre_info'>Mes infos </p>
                                    <div className="Mes_infos_contenu">
                                        <p className="">Nom : {contact.lastname}</p>
                                        <p className="">Prénom : {contact.firstname}</p>
                                        <p className="">Email : {contact.mail}</p>
                                        <p className="">Téléphone : {contact.phone}</p>
                                    </div>
                                </div>
                                <div className="Alertes">
                                    <p className='titre_alertes'>Alertes </p>
                                    <div className="Alertes_contenu">
                                    { data12.length != 0 ? (data12[8].total+data12[9].total+data12[10].total)/3 > data12[11].total ? <div >Le chiffre d'affaire de ce mois est en baisse </div> : <div>Le chiffre d'affaire de ce mois est en hausse</div> : <div></div>}
                                    </div>
                                </div>
                            </div>
                            <div className="_bas">
                                <div className="Mes_prochaines_activités">
                                    <p className='titre_activites'>Mes prochaines activités </p>
                                </div>
                                <div className="Chiffre_clés">
                                    <div className="Ch_Clé_gauche">
                                        <div className="Ch_Clé_gauche_haut">
                                            <p className='titre_chiffre'>Chiffre clés</p>
                                            <div>{dataKey.total}</div>
                                        </div>
                                        <div className="Ch_Clé_gauche_bas">
                                            <p className='titre_chiffre'>Chiffre clés</p>
                                            <div>{dataKey.totalcontact}</div>
                                        </div>
                                    </div>
                                    <div className="Ch_Clé_droite">
                                        <div className="Ch_Clé_droite_haut">
                                            <p className='titre_chiffre'>Chiffre clés</p>
                                            <div>{infoBestCustomer.name + " : " + infoBestCustomer.total}</div>
                                        </div>
                                        <div className="Ch_Clé_droite_bas">
                                            <p className='titre_chiffre'>Chiffre clés</p>
                                            <div>{new Date().getFullYear() + " : " + (new Date().getMonth() + 1)}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="_droite">
                            <div className="clients_important">
                                <p className='titre_clients'>Clients importants</p>
                            </div>
                            <div className="contacts_ajouté_récemment">
                                <p className='titre_contacts'>Contacts ajoutés récemment</p>
                                {infoContactRecent.map(info => (
                                    <div>{info.firstname + " " + info.lastname}</div>
                                ))}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </body>
    );
};

export default Dashboard;