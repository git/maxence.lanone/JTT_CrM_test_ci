import { Component } from '@fullcalendar/core';
import userEvent from '@testing-library/user-event';
import React, { useEffect, useRef, useState } from 'react';
import NavigationDashboard from '../components/NavigationDashboard';
import img1 from '../img/logo_personEntouré.svg';
import axios from 'axios'
import Session from 'react-session-api';
import { render } from '@testing-library/react';

const api = axios.create({
    baseURL: 'http://localhost:8080'
})

function Compte() {
    const [theme, setTheme] = useState("light");
    const nomInputRef = useRef();
    const prenomInputRef = useRef();
    const [modificationInfo, setModificationInfo] = useState(false);
    const [selectedFile, setSelectedFile] = useState([]);
    const [fileBase64String, setFileBase64String] = useState("");
    const [lastName, setLastName] = useState("");
    const [firstName, setFirstName] = useState("");
    const [phone, setPhone] = useState("");
    const [mail, setMail] = useState("");
    const [image, setImage] = useState("");
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const [formData, setFormData] = useState();


    function handleFileChange(event) {
        const file = event.target.files[0];
        if (!file) {
            return;
        }

        // Créez un nouvel objet FileReader
        const fileReader = new FileReader();

        // Définissez une fonction de rappel pour être exécutée lorsque la lecture du fichier est terminée
        fileReader.onload = () => {
            // Obtenez les données du fichier en base64
            const base64Data = fileReader.result;
            document.getElementById('imgCompte').src = base64Data;
            setImage(base64Data);

            api.put('User/Update/Image/' + Session.get("idUser"), { image: base64Data }).then(response => {
                // Traitez la réponse du serveur
                setLoading(false);
            });

        };

        // Démarrez la lecture du fichier
        fileReader.readAsDataURL(file);
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        // envoyer les données modifiées au serveur ici
        api.put('/User/UpdateByClient/' + Session.get("idUser"), formData).then(response => {
            // Traitez la réponse du serveur
        });
        setModificationInfo(false);
        setLastName(formData.lastName);
        setFirstName(formData.firstName);
        setPhone(formData.phone);
        setMail(formData.mail);
    }
    
    const handleChange = (event) => {
        setFormData({...formData, [event.target.name]: event.target.value });
    }



    if (localStorage.getItem('theme') && localStorage.getItem("theme") !== '' && localStorage.getItem("theme") !== theme) {
        setTheme(localStorage.getItem("theme"))
    }


    useEffect(() => {
        
        const apiString = '/User/Id/' + Session.get("idUser");
        api.get(apiString).then((response) => {
            setLastName(response.data[0].lastname);
            setFirstName(response.data[0].firstname);
            setPhone(response.data[0].phone);
            setMail(response.data[0].mail);
            setImage(response.data[0].image);
        });
    }, []);

    useEffect(() => {
        setFormData({
            firstName: firstName,
            lastName: lastName,
            mail: mail,
            phone: phone,
        })
    }, [null, modificationInfo]);



    return (
        <body className={theme}>

            <link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.0/css/line.css"></link>

            <div className="page_compte">
                {/* Create an account page */}
                <div className="haut_de_page">
                    <h2 className="titre">Mon Compte</h2>
                    <div className="rechLogo">
                        <img className="logo" srcSet={theme === "light" ? './LogoApp.svg' : './LogoApp_light.svg'} />
                    </div>
                </div>
                <div className="bas_de_page">
                    <NavigationDashboard />
                    <form className="Compte" onSubmit={handleSubmit}>
                        <div className="name_picture">
                            <div className="picture">
                                <div id="display_image">
                                    <img src={image} id="imgCompte" className="img" />
                                    <form id="hiddenInput">
                                        <input name="image64" id="fileUpload" type="file" onChange={handleFileChange}></input>
                                        <label className="fileUpload" htmlFor="fileUpload">Ajoutez votre photo</label>
                                    </form>
                                </div>

                                
                            </div>
                            
                            <div className="name">
                                <div className="presentationNom">
                                    <p id="texte" className="def">Nom Complet :</p>
                                    {modificationInfo ?
                                    <div className="changeInput">
                                        <input
                                            name="firstName"
                                            placeholder="firstName"
                                            value={formData.firstName}
                                            onChange={handleChange}
                                            className="inputCompte"
                                        />
                                        <input
                                            name="lastName"
                                            placeholder="lastName"
                                            value={formData.lastName}
                                            onChange={handleChange}
                                            className="inputCompte"
                                        />
                                    </div>
                                    :
                                        <p>{firstName + " " + lastName}</p>
                                    }
                                </div>
                                <div className='bouton_submit'>
                                    {modificationInfo ?
                                    <div>
                                        <div id="bouton" className="bouton_modifierNom" onClick={() => setModificationInfo(false)}>Annuler</div>
                                        <button id="bouton" className="bouton_modifierNom" type='submit' >Valider</button>
                                    </div>
                                    :
                                    <div id="bouton" className="bouton_modifierNom" onClick={() => setModificationInfo(true)}>Modifier</div>
                                    }
                                </div>
                            </div>
                        </div>
                        <div className="infoPerso">
                            <p className="description">Informations personnelles</p>
                            <div className="parti_mail">
                                <p className="def">Mail :</p>
                                {modificationInfo ?
                                    <input
                                        name="mail"
                                        placeholder={mail}
                                        value={formData.mail}
                                        onChange={handleChange}
                                    />
                                :
                                    <input id="mail" value={mail} name="mail" className="mail" type="text" disabled />
                                }
                            </div>
                            <div className="parti_pays">
                                <p className="def">Pays ou Région :</p>
                                <p className="pays_region">France</p>
                            </div>
                            <div className="parti_tel">
                                <p className="def">Tel :</p>
                                {modificationInfo ?
                                    <input
                                        name="phone"
                                        placeholder={phone}
                                        value={formData.phone}
                                        onChange={handleChange}
                                    />
                                :
                                    <input id="phone" value={phone} name="tel" className="tel" type="text" disabled />
                                }
                                
                            </div>
                        </div>
                        <div className="infoEntreprise">
                            <p className="description">Informations entreprise</p>
                            <div className="parti_name">
                                <p className="def">Nom de l'entreprise</p>
                                <p className="name">entreprise name</p>
                            </div>
                            <div className="parti_secteurAct">
                                <p className="def">Secteur d'activité :</p>
                                <p className="secteurAct">Coiffure</p>
                            </div>
                            <div className="parti_nbClient">
                                <p className="def">Nombre clients :</p>
                                <p className="nbClient">200</p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </body >
    );
};



export default Compte;